/*
 * Items: chroot(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <unistd.h>

main(int arg, char **argv)
{
    (void)chroot("/");
}
