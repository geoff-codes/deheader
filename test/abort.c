/*
 * Items: abort(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <stdlib.h>

main(int arg, char **argv)
{
    abort();
}
