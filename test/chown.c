/*
 * Items: chown(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <sys/types.h>
#include <unistd.h>

main(int arg, char **argv)
{
    (void)chown("/", 0, 0);
}
